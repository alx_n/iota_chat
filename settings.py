from iota import Tag
from helpers import get_healthy_node

# required - Main IOTA node
# iota_node_main = 'http://localhost:14265'
iota_node_main = get_healthy_node()
print("Using node %s" % iota_node_main)

# optional - IOTA node for doing Proof-of-Work. If empty, iota_node_main will be used for PoW
iota_node_pow = 'http://localhost:14265'

# target address for sending & reading transactions. Think of it as a chat room.
target_addr = b'HLZEWQEOYQQHEAUFBBEKR99ZXHKVHIJJN9ULLSYCKMNKECBHQGBIORYZLAJ9VTOLBUEOWLDGABJ9XQBP9GXYEBCKDA'
# dev hash for testing KD9WMXNSHROEKSNX9FHNYNELSKNZNQKEHFIDHSNQLSIFNTHWISNZAHEIFHSXMD9EWHNAMZNDOWURHFKSD

# your nickname. Keep it short to generate less transactions per message.
# Not protected in any way, anybody can set any name
nickname = 'dev'

# transaction tag, doesn't really matter for now
transaction_tag = Tag(b'BASIC9Q9CHAT')

# Reduce only if you use your own node.
# Respect the owners of public nodes, they do not receive any profits for it, and 30 sec is already questionable
response_read_interval = 30
