from collections import defaultdict
import datetime

from iota import Transaction, Bundle


class ResponseReader(object):

    def __init__(self, api, target_addr):
        self.api = api
        self.target_addr = target_addr
        self.previous_bundles = dict()

    def print_new_responses(self):

        transactions_response = self.api.find_transactions(
            addresses=[self.target_addr],
            # tags=[transaction_tag],
        )
        # print("transactions_response duration: %s" % transactions_response.get("duration"))
        trans_hashes = transactions_response.get('hashes', [])

        received_transactions = defaultdict(list)

        if trans_hashes:
            trytes_response = self.api.get_trytes(transactions_response.get('hashes', []))
            # print("trytes_response duration: %s" % trytes_response.get("duration"))
            for trans_tryte in trytes_response.get("trytes", []):
                transaction = Transaction.from_tryte_string(trans_tryte)
                if not transaction.bundle_hash in self.previous_bundles:
                    received_transactions[transaction.bundle_hash].append(transaction)

        transactions_sorted_by_timestamp = sorted(received_transactions.items(), key=lambda k_v: k_v[1][0].timestamp)

        for bundle_hash, transactions_list in transactions_sorted_by_timestamp:
            bundle = Bundle()
            bundle.transactions = transactions_list
            timestamp = datetime.datetime.fromtimestamp(transactions_list[0].timestamp).strftime('%Y-%m-%d %H:%M:%S')

            if not bundle_hash in self.previous_bundles:
                self.previous_bundles[bundle_hash] = bundle

            try:
                messages = bundle.get_messages(errors='replace')
                if messages:
                    splitted_message = messages[0].split(';')
                    if len(splitted_message) >= 2:
                        nickname = splitted_message[0]
                        msg = messages[0][len(nickname)+1:]
                        print("[%s] <%s> %s" % (timestamp, nickname, msg))
                    else:
                        print("[%s] MALFORMED: %s" % (timestamp,  messages[0]))
                else:
                    print("[%s] %s" % (timestamp,  "SYSTEM: EMPTY MESSAGE"))
            except UnicodeEncodeError as e:
                print("[%s ]%s" % (timestamp,  "SYSTEM: DECODE ERROR"))
