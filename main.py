# coding=utf-8
import math

from iota.adapter.wrappers import RoutingWrapper
from iota.adapter import BadApiResponse
from iota.api import Iota
from iota.transaction.creation import ProposedTransaction
from iota.transaction.types import Fragment
from iota.types import TryteString, Address

from iota_patching import iota_lib_monkeypatch
import settings
from response_read import ResponseReader
from settings import transaction_tag, target_addr, response_read_interval, nickname
from threads import call_repeatedly

iota_lib_monkeypatch()

if settings.iota_node_pow:
    api = Iota(
        RoutingWrapper(settings.iota_node_main)
        .add_route('attachToTangle', settings.iota_node_pow)
        .add_route('interruptAttachingToTangle', settings.iota_node_pow)
    )
else:
    api = Iota(RoutingWrapper(settings.iota_node_main))

reader = ResponseReader(api, target_addr)

reader.print_new_responses()
cancel_response_reading = call_repeatedly(response_read_interval, reader.print_new_responses)
# call cancel_response_reading() to, uhm, cancel it

while True:
    print('You can enter a message now')
    raw_message = input()
    print('Processing the message...')

    message = TryteString.from_string(nickname + ";" + raw_message)
    print('Requiring transactions: %s' % int(math.ceil(float(len(message)) / float(Fragment.LEN))))

    try:
        transfer_resp = api.send_transfer(
            depth=100,
            transfers=[
                ProposedTransaction(
                    address=Address(target_addr),
                    value=0,
                    tag=transaction_tag,
                    message=message,
                ),
            ],
        )
        print("bundle_hash: %s" % transfer_resp["bundle"].hash)

    except BadApiResponse as e:
        print("SYSTEM: Node Error: %s" % e)
