import requests


def get_healthy_node():
    # all credits go to https://iota.dance/
    node_stats_request = requests.get('https://iota.dance/data/node-stats')
    json_stats = node_stats_request.json()
    for node in json_stats:
        if node.get("health") == 10 and node.get("index") - node.get("solid") <= 5:
            return "%s:%s" % (node.get("node"), node.get("port"))
