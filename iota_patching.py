# Monkeypatching AttachToTangle
import iota
from iota import TryteString, TrytesDecodeError
from iota.commands.core.attach_to_tangle import AttachToTangleRequestFilter, AttachToTangleResponseFilter


def iota_lib_monkeypatch():
    def _att_get_request_filter(self):
        print("SYSTEM: Doing PoW...")
        return AttachToTangleRequestFilter()

    iota.commands.core.attach_to_tangle.AttachToTangleCommand.get_request_filter = _att_get_request_filter

    def _att_get_response_filter(self):
        print("SYSTEM: PoW done")
        return AttachToTangleResponseFilter()

    iota.commands.core.attach_to_tangle.AttachToTangleCommand.get_response_filter = _att_get_response_filter

    def _bundle_get_messages(self, errors='drop'):
        decode_errors = 'strict' if errors == 'drop' else errors
        messages = []
        for group in self.group_transactions():
            if group[0].value < 0:
                continue
            message_trytes = TryteString(b'')
            for txn in sorted(group, key=lambda tx: tx.current_index):  # KEY LINE, sort transactions by index before appending
                message_trytes += txn.signature_message_fragment
            if message_trytes:
                try:
                    messages.append(message_trytes.as_string(decode_errors))
                except (TrytesDecodeError, UnicodeDecodeError):
                    if errors != 'drop':
                        raise
        return messages

    iota.transaction.base.Bundle.get_messages = _bundle_get_messages
